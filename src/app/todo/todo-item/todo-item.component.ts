import {Component, ElementRef, Input, OnInit, ViewChild} from '@angular/core';
import { Todo } from '../model/todo.model';
import { FormControl, Validators } from '@angular/forms';
import {Store} from '@ngrx/store';
import {AppState} from '../../app.reducers';
import {DeleteTodoAction, EditTodoAction, ToggleTodoAction} from '../todo.actions';

@Component({
  selector: 'app-todo-item',
  templateUrl: './todo-item.component.html',
  styles: []
})
export class TodoItemComponent implements OnInit {
  @Input() todo: Todo;
  @ViewChild('inputEditText') inputEditText: ElementRef;

  public chkField: FormControl;
  public textInput: FormControl;
  public editing: boolean;

  constructor(private store: Store<AppState>) { }

  ngOnInit() {
    this.chkField = new FormControl(this.todo.completed);
    this.textInput = new FormControl(this.todo.text, Validators.required);

    this.chkField.valueChanges.subscribe(value => {
      const action = new ToggleTodoAction(this.todo.id);
      this.store.dispatch(action);
    });

  }

  public edit(): void {
    this.editing = true;
    setTimeout(() => {
        this.inputEditText.nativeElement.select();
    }, 1);
  }

  public finishedEdit(): void {
    this.editing = false;
    if ((this.textInput.valid) && (this.textInput.value !== this.todo.text)) {
        const action = new EditTodoAction(this.todo.id, this.textInput.value);
        this.store.dispatch(action);
    }
  }

  public deleteTodo(): void {
    const action = new DeleteTodoAction(this.todo.id);
    this.store.dispatch(action);
  }

}
