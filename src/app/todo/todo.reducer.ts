import * as fromTodo from './todo.actions';
import {Todo} from './model/todo.model';

const todo1 = new Todo('Aprender a programar en python');
const todo2 = new Todo('Aprender a programar en c#');
const todo3 = new Todo('Aprender el framework React native');

todo2.completed = true;

const stateStart: Todo[] = [todo1, todo2, todo3];

export function todoReducer(state = stateStart, action: fromTodo.Accions): Todo[] {
    switch (action.type) {
        case fromTodo.ADD_TODO:
            const todo = new Todo(action.text);
            return [...state, todo];
        case fromTodo.TOGGLE_TODO:
            return state.map(todoEdit => {
                if (todoEdit.id === action.id) {
                    return {
                        ...todoEdit,
                        completed: !todoEdit.completed
                    };
                } else {
                    return todoEdit;
                }
            });
        case fromTodo.TOGGLE_ALL_TODO:
            return state.map(todoEdit => {
                return {
                    ...todoEdit,
                    completed: action.completed
                };
            });
        case fromTodo.EDIT_TODO:
            return state.map(todoEdit => {
                if (todoEdit.id === action.id) {
                    return {
                        ...todoEdit,
                        text: action.text
                    };
                } else {
                    return todoEdit;
                }
            });
        case fromTodo.DELETE_TODO:
            return state.filter(todoDelete => todoDelete.id !== action.id);
        case fromTodo.DELETE_ALL_TODO:
            return state.filter(todoDelete => !todoDelete.completed);
        default:
            return state;
    }
}
