import {Todo} from './todo/model/todo.model';
import {ActionReducerMap} from '@ngrx/store';
import * as fromTodo from './todo/todo.reducer';
import * as fromFilter from './todo/filter/filter.reducer';
import * as fromFilterAction from './todo/filter/filter.actions';

export interface AppState {
    todos: Todo[];
    filter: fromFilterAction.filterValid;
}

export const AppReducers: ActionReducerMap<AppState> = {
    todos: fromTodo.todoReducer,
    filter: fromFilter.filterReducer
};
